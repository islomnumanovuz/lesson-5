// ! 1
function vowelCount(str) {
  let vowels = ["a", "e", "i", "o", "u"];
  return str.split("").reduce((acc, curr) => {
    if (vowels.includes(curr)) {
      return acc + 1;
    }
    return acc;
  }, 0);
};
// console.log(vowelCount("The quick brown fox"));
//-------------------------------------------------
//-------------------------------------------------
// ! 2
function daysTillNewYear() {
  let now = new Date().getTime();
  let newYear = new Date(`${new Date().getFullYear() + 1}/01/01`).getTime();
  let diff = Math.round((newYear - now) / 1000 / 3600 / 24);
  return `Yangi yilgacha ${diff} kun qoldi`;
}
console.log(daysTillNewYear());
//-------------------------------------------------
//-------------------------------------------------
// ! 3
function findSymbol(arr) {
  return arr.filter((el) => {
    return (
      (`${el}`.charCodeAt() < 48 || `${el}`.charCodeAt() > 57) &&
      (`${el}`.charCodeAt() < 65 || `${el}`.charCodeAt() > 90) &&
      (`${el}`.charCodeAt() < 97 || `${el}`.charCodeAt() > 122)
    );
  });
}
// console.log(findSymbol(["@", "#", "%", "a"]));
//-------------------------------------------------
//-------------------------------------------------
// ! 4
function intersection(arr1, arr2) {
  return arr1.reduce((acc, curr) => (arr2.includes(curr) ? curr : acc));
}
// console.log([1,2,3,3,1]);
//------------------------------------------------
// ! 5
function notify(msg) {
  console.log(setTimeout(() => alert(msg), 4000));
};
// notify("Hello bro");
//-------------------------------------------------
//-------------------------------------------------
// ! 6
function sumNatural(arr) {
  return arr.reduce((acc, curr) => {
    if (typeof curr == "number" && curr > 0 && curr % 1 == 0) {
      return acc + curr;
    }
    return acc;
  }, 0);
};
// console.log(sumNatural([1,2,3,4]));
//-------------------------------------------------
//-------------------------------------------------
// ! 7
function charCount(str, char) {
  let counter = 0;
  str.split("").forEach((element) => {
    if (element == char) counter++;
  });
  return counter;
};
console.log(charCount("Islom", "islom"));
//-------------------------------------------------
//-------------------------------------------------

